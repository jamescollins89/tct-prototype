import React, { useState } from "react";
import AppData from '../Data/AppData.json';
import FormDonationButtons from './FormDonationButtons';
import FormDonationAmounts from "./FormDonationAmounts.js";

function FormBuild() {

    const DONATIONS = AppData.donations;
    const [stateDonationType, setDonationType] = useState(DONATIONS[0]);
    const [stateDonationArray, setDonationArray] = useState(stateDonationType.values[1]);
    const [stateDonationValue, setDonationValue] = useState(stateDonationType.values[1].amount);

    function handleClick(donation) {
        setDonationType(donation);
        if (donation.type === 'single'){
            setDonationArray(donation.values[1]);
            setDonationValue(donation.values[1].amount);
        } else if (donation.type === 'monthly') {
            setDonationArray(donation.values[0]);
            setDonationValue(donation.values[0].amount);
        }
    }

    function handleChange(e, data) {
        if (data.type === 'checkbox'){
            setDonationArray(stateDonationType.values[data.value]);
            setDonationValue(stateDonationType.values[data.value].amount);
        } else {
            setDonationValue(data.value);
        }
    }
    
    function handleDonateNow(){
        console.log('donating..')
    }

    function handleGetState() {
        console.log( stateDonationType, stateDonationValue );
    }

    return (
        <div>
            {DONATIONS.map(donation => (
                <FormDonationButtons
                    donation={donation}
                    donationtype={stateDonationType}
                    handleClick={handleClick}
                    key={donation.id}
                />
            ))}
            <FormDonationAmounts
                donationtype={stateDonationType}
                donationarray={stateDonationArray}
                donationvalue={stateDonationValue}
                handleDonateNow={handleDonateNow}
                handleChange={handleChange}
            />
            <p>&nbsp;&nbsp;&nbsp;&nbsp;</p>
            <button onClick={handleGetState}>Get State</button>
        </div>
    );
}

export default FormBuild;