import React, { useState } from "react";
import { Grid, Checkbox, Input, Button } from 'semantic-ui-react';

function FormDonationAmounts(props) {

    const [otherSelected, setotherSelected] = useState(false);

    function handleChange(e, data) {
        props.handleChange(e, data);
        if (data.type === 'checkbox'){
            setotherSelected(false);
        }
    }

    function handleOther(e) {
        setotherSelected(true);
    }

    return (
        <Grid centered>
            <Grid.Row>
                <h3>Donation Amounts</h3>
            </Grid.Row>
            <Grid.Row>
                {props.donationtype.values.map(function(data, i){
                return (
                    <Grid.Column key={i}>
                        <Checkbox radio label={data.label} value={i} name="donationValue" checked={props.donationvalue === data.amount && !otherSelected} onChange={handleChange} />
                    </Grid.Column>
                )
                })}
                <Grid.Column>
					<Checkbox radio label="Other" name="customDonation" checked={otherSelected} onChange={handleOther} />
				</Grid.Column>
            </Grid.Row>
            {/* Only show this if otherSelected is true: */}
            {otherSelected ?
                <Grid.Row>
                    <Grid.Column>
                        <div>How much would you like to donate</div>
                        <Input defaultValue="0" name="donationValue" onChange={handleChange} />
                    </Grid.Column>	
                </Grid.Row>
            : // <- Don't remove this
                <Grid.Row textAlign='center'>
                    <Grid.Column>
                        <h1 style={{display: 'inline'}}>£{props.donationarray.amount} </h1>
                        {props.donationarray.description}
                    </Grid.Column>
                </Grid.Row>
            }
            <Grid.Row>
                <Grid.Column>
                    <Button fluid onClick={props.handleDonateNow}>Donate Now</Button>
                </Grid.Column>
            </Grid.Row>

        </Grid>
    );
}

export default FormDonationAmounts;