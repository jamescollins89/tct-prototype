import React, { Component } from 'react';
import FormDonationAmount from './FormDonationAmountBackup';
import FormStepChoice from './FormStepChoice';
import Error from './Error';

class FormBuild extends Component {

    state = {
        step: 1,
        customDonationVisible: false,
        donationValue: 25,
        donationType: 'single',
        donationOption: '',
    };

    // Handle input change
    handleChange = (e, data) => {
        //e.preventDefault();
        // console.log(e);
        // console.log(data);
        if (data.type === 'checkbox'){
            this.setState({
                customDonationVisible: false,
                [data.name]: data.value
            });
        } else {
            this.setState({
                [data.name]: data.value
            });
        }
    }

    updateDonationType = (data) => {
        this.setState({
            donationType: data
        });
    }

    toggleDonationInput = () => {
        console.log('toggleDonationInput');
		this.setState({
            customDonationVisible: !this.state.customDonationVisible,
            donationValue: 0
		});
	}

    handleGetState = () => {
        console.log(this.state);
    }

    // Proceed to Next Step
    nextStep = () => {
        const { step } = this.state;
        this.setState({
            step: step + 1
        });
    };

    // Return to Previous Step
    prevStep = () => {
        const { step } = this.state;
        this.setState({
            step: step - 1
        });
    };

    render() {
        const { step } = this.state;
        const { donationValue, customDonationVisible, donationType, donationOption } = this.state;
        const values = { donationValue, customDonationVisible, donationType, donationOption };

        switch (step) {
            case 1:
            return (
                <FormDonationAmount
                    handleChange={this.handleChange}
                    handleGetState={this.handleGetState}
                    toggleDonationInput={this.toggleDonationInput}
                    updateDonationType={this.updateDonationType}
                    nextStep={this.nextStep}
                    values={values}
                />
            );
            case 2:
            return (
                <FormStepChoice
                    handleChange={this.handleChange}
                    handleGetState={this.handleGetState}
                    nextStep={this.nextStep}
                    prevStep={this.prevStep}
                    values={values}
                />
            );
            default:
                return (
                    <Error />
                );
        }
    }
}
 
export default FormBuild;