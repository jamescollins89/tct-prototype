import React, { Component } from 'react';

class Error extends Component {
    render() { 
        return (
            <h6>Whoops, something went wrong.</h6>
        );
    }
}
 
export default Error;