import React from "react";
import { Button } from 'semantic-ui-react';

function FormDonationButtons(props) {
    
    function handleClick() {
        props.handleClick(props.donation);
    }

    const backgroundColour = props.donationtype.id === props.donation.id ? "red" : "blue";

    const style = {
        backgroundColor: backgroundColour
    };

    return (
        <Button key={props.donation.id} style={style} onClick={handleClick}>
            {props.donation.label} {props.donation.values.amount}
        </Button>
    );
}

export default FormDonationButtons;