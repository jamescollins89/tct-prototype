import React, { Component } from 'react';
import { Grid, Checkbox, Button } from 'semantic-ui-react';
import FormIndividual from './FormIndividual';
import FormEvent from './FormEvent';
import FormCompany from './FormCompany';

class FormStepChoice extends Component {

	continue = e => {
        e.preventDefault();
        console.log(this.props.values);
        this.props.nextStep();
    };
    
    back = e => {
        e.preventDefault();
        this.props.prevStep();
    };
	
	render() {
		const { values, handleChange, handleGetState } = this.props;
		return (
			<Grid>
				<Grid.Row columns={2}>
                    <Grid.Column>
                        {values.donationType} £{values.donationValue}
                    </Grid.Column>
                    <Grid.Column>
                        <span onClick={this.back}>Edit amount</span>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
					<Grid.Column>
						<h3>Tell us a little more about your donation</h3>
					</Grid.Column>
				</Grid.Row>
				<Grid.Row columns={3}>
					<Grid.Column>
						<Checkbox radio label="My own money" value="ownMoney" name="donationOption" checked={values.donationOption === 'ownMoney'} onChange={handleChange} />
					</Grid.Column>
					<Grid.Column>
                        <Checkbox radio label="Event money" value="eventMoney" name="donationOption" checked={values.donationOption === 'eventMoney'} onChange={handleChange} />
					</Grid.Column>
					<Grid.Column>
                        <Checkbox radio label="Company money" value="companyMoney" name="donationOption" checked={values.donationOption === 'companyMoney'} onChange={handleChange} />
					</Grid.Column>
				</Grid.Row>

                {values.donationOption === 'ownMoney' && 
                    <FormIndividual />
                }
                {values.donationOption === 'eventMoney' && 
                    <FormEvent />
                }
                {values.donationOption === 'companyMoney' && 
                    <FormCompany />
                }
				<Grid.Row centered columns={2}>
					<Grid.Column>
						<Button fluid onClick={handleGetState}>Get State</Button>
					</Grid.Column>
					<Grid.Column>
						<Button fluid onClick={this.continue}>Continue</Button>
					</Grid.Column>
				</Grid.Row>
			</Grid>
		);
	}
}

export default FormStepChoice;
