import React, { Component, useState } from 'react';
import { Grid, Checkbox, Input, Button } from 'semantic-ui-react';
import AppData from '../Data/AppData.json';

class FormDonationAmount extends Component {
	continue = e => {
        e.preventDefault();
        console.log(this.props.values);
        this.props.nextStep();
	};
	
	render() {

		const { values, handleChange, handleGetState, toggleDonationInput, updateDonationType } = this.props;

		console.log(AppData.donations);

		const donationOptions = AppData.donations.map((data, i) => 
			<Grid.Column key={i}>
				<Button>{data.label}</Button>
			</Grid.Column>
		);

		const donationItems = AppData.donations[0].values.map((data, i) => 
			<Grid.Column key={i}>
				<Checkbox radio label={data.label} value={data.amount} name="donationValue" checked={values.donationValue === data.amount} onChange={handleChange} />
			</Grid.Column>
		);

		return (
			<Grid>
				<Grid.Row>
					<Grid.Column>
						<h3>Help us provide...</h3>
					</Grid.Column>
				</Grid.Row>
				<Grid.Row columns={2}>
					{donationOptions}
				</Grid.Row>
				<Grid.Row columns={4}>
					{donationItems}
					<Grid.Column>
						<Checkbox radio label="Other" name="customDonation" checked={values.customDonationVisible} onChange={toggleDonationInput} />
					</Grid.Column>
				</Grid.Row>

				{/* Only show this if customDonation has been selected: */}
				{values.customDonationVisible && 
					<Grid.Row>
						<Grid.Column>
							<div>How much would you like to donate</div>
							<Input defaultValue="0" name="donationValue" onChange={handleChange} />
						</Grid.Column>	
					</Grid.Row>
				}

				{/* Only show this if customDonation is not visible: */}
				{!values.customDonationVisible &&
				<Grid.Row textAlign='center'>
					<Grid.Column>
						<h1 style={{display: 'inline'}}>£{values.donationValue} </h1>
						<span>
						{values.donationValue === '10' &&
							<React.Fragment>
								£10 would do this for the charity...
							</React.Fragment>
						}
						{values.donationValue === '25' &&
							<React.Fragment>
								£25 would do this for the charity...
							</React.Fragment>
						}
						{values.donationValue === '60' &&
							<React.Fragment>
								£60 would do this for the charity...
							</React.Fragment>
						}
						</span>
					</Grid.Column>
				</Grid.Row>
				}

				<Grid.Row>
					<Grid.Column>
						{values.donationValue ? '0' &&
						<Button fluid onClick={this.continue}>Donate Now</Button>
						:
						<Button fluid disabled onClick={this.continue}>Donate Now</Button>
						}
					</Grid.Column>
				</Grid.Row>
				<Button fluid onClick={handleGetState}>Get State</Button>
			</Grid>
		);
	}
}

export default FormDonationAmount;
