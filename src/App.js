import React from 'react';
import './App.css';
import FormBuild from './Components/FormBuild';

function App() {
  return (
    <div className="App">
      <FormBuild />
    </div>
  );
}

export default App;
